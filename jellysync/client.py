import os
from pathlib import Path
from queue import Queue
import shutil

import requests
import uuid

URL = "https://jf.rubdos.be"
API_KEY = ""
COLLECTION_ID = ""
USER_ID = ""

SYNC_TARGET_DIR = "./dump/"


def sync():
    client = Client(URL, API_KEY)
    items = Queue()
    for item in client.collection(USER_ID, COLLECTION_ID):
        items.put(item)

    sync = Synchroniser(SYNC_TARGET_DIR)
    sync.gather()

    while not items.empty():
        item = items.get()
        if item["Type"] == "Season":
            for sub_item in client.items_with_parent(USER_ID, item["Id"]):
                items.put(sub_item)
        elif item["Type"] == "Series":
            for sub_item in client.items_with_parent(USER_ID, item["Id"]):
                items.put(sub_item)
        else:
            if not sync.schedule(item):
                print("Warning, could not schedule", item)

    to_remove = sync.compute_difference()
    for item in to_remove:
        os.unlink(item)
        # shutil.rmtree(item)

    params = {
        "PlaySessionId": str(session),
        "Static": "false",
        "VideoCodec": "h264",
        "VideoBitrate": "960000",
        "Framerate": "25",
        "MaxWidth": "640",
        "MaxHeight": "480",
        "SubtitleMethod": "Encode",
        "AudioCodec": "aac",
        "AudioChannels": "2",
        "Profile": "high",
        "Level": "3",
    }

    for item in sync.scheduled:
        session = uuid.uuid4()
        params["PlaySessionId"] = str(session)
        path = sync.path_for(item)
        dir = sync.dir_for(item)
        os.makedirs(dir, exist_ok=True)

        print("Fetching", path)
        with client.stream_video(item["Id"], params) as video:
            assert video.status_code == 200
            try:
                with open(path, 'wb+') as f:
                    for b in video.iter_content():
                        f.write(b)
            except Exception as e:
                print(e, "removing file")
                os.unlink(path)

    # TODO: cleanup empty dirs


class Synchroniser(object):
    def __init__(self, base_path):
        self.base_tv = Path(base_path + "/TV")
        self.base_movies = Path(base_path + "/Movies")
        self.scheduled = []

        assert self.base_tv.exists()
        assert self.base_movies.exists()

    def gather(self):
        self.movies = []
        self.tv = []

        for movie_path in self.base_movies.iterdir():
            if movie_path.is_dir():
                for file_path in movie_path.iterdir():
                    self.movies.append(file_path)

        for series_path in self.base_tv.iterdir():
            if series_path.is_dir():
                for season_path in series_path.iterdir():
                    if season_path.is_dir():
                        for episode_path in season_path.iterdir():
                            self.tv.append(episode_path)

    def schedule(self, item):
        if item["Type"] not in ["Movie", "Episode"]:
            return False
        self.scheduled.append(item)
        return True

    def dir_for(self, item):
        if item["Type"] == "Episode":
            target = (self.base_tv
                      / item["SeriesName"]
                      / item["SeasonName"])
            return Path(target)
        elif item["Type"] == "Movie":
            target = (self.base_movies
                      / item["Name"])
            return Path(target)
        else:
            print("Unreachable, cannot process", item)

    def path_for(self, item):
        if item["Type"] == "Episode":
            episode = ""
            if "IndexNumber" in item:
                episode = "E" + str(item["IndexNumber"])
            if "Name" in item:
                episode += " " + item["Name"]
            target = (self.base_tv
                      / item["SeriesName"]
                      / item["SeasonName"]
                      / (item["SeriesName"] +
                         " S" + str(item["ParentIndexNumber"]) +
                         episode + ".mp4"))
            return Path(target)
        elif item["Type"] == "Movie":
            target = (self.base_movies
                      / item["Name"]
                      / (item["Name"] + ".mp4"))
            return Path(target)
        else:
            print("Unreachable, cannot process", item)

    # Remove from the schedule what we already have,
    # return from the indexed paths what we have but don't need.
    def compute_difference(self):
        # Copy the present list
        present = self.movies[:] + self.tv[:]

        # Remove from the lists what is not scheduled
        to_unschedule = []
        for item in self.scheduled:
            path = self.path_for(item)
            if path is None:
                continue

            if path in present:
                # It's scheduled, we already have it, don't remove it,
                # don't download it either.
                present.remove(path)
                to_unschedule.append(item)
        for item in to_unschedule:
            self.scheduled.remove(item)

        return present


class Client(object):
    def __init__(self, base_url, token):
        self.base_url = base_url
        self.session = requests.Session()
        self.session.headers.update({'X-Emby-Token': token})

    def playlist(self, id):
        url = self.base_url + "/Playlists/" + id + "/Items"
        return self.session.get(url).json()["Items"]

    def items_with_parent(self, user, id):
        url = self.base_url + "/Users/" + user + "/Items?ParentId=" + id
        return self.session.get(url).json()["Items"]

    def collection(self, user, id):
        return self.items_with_parent(user, id)

    def stream_video(self, id, extras={}):
        url = self.base_url + "/Videos/" + id + "/stream.mp4?"
        for key, val in extras.items():
            url += key + "=" + val + "&"
        return self.session.get(url, stream=True)
